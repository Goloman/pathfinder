#pragma once

#include <glm/glm.hpp>
#include <vector>
#include <string>

struct Patch {
	std::vector<glm::vec3> points;
	size_t width;
	size_t height;
	bool wrapped;
	bool smooth;
};

std::vector<Patch> LoadModel(std::string filename, glm::mat4 transform);

glm::vec3 Evaluate(Patch patch, glm::vec2 uv);
glm::vec3 EvaluateDu(Patch patch, glm::vec2 uv);
glm::vec3 EvaluateDv(Patch patch, glm::vec2 uv);
glm::vec3 EvaluateNormal(Patch patch, glm::vec2 uv);
glm::vec3 EvaluateNormal2(Patch patch, glm::vec2 uv);
glm::vec3 EvaluateDistant(Patch patch, glm::vec2 uv, float radius);
glm::vec3 EvaluateDistant2(Patch patch, glm::vec2 uv, float radius, float bias);

std::vector<glm::vec3> Intersect(Patch p1, Patch p2, float radius, float bias1, float bias2, glm::vec3 start, int count, bool both = false, int skip = 0, int count2 = 0);
