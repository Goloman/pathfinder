CXXFLAGS = -O2 -g

pathfinder: *.cpp *.hpp
	g++ $(CXXFLAGS) *.cpp -o pathfinder

.PHONY: clean test view

clean:
	rm -f pathfinder out.png 1.k16 2.f12

test: pathfinder darek.json
	./pathfinder darek.json
	cp 1.k16 ../cameo/
	cp 2.f12 ../cameo/
	cp 3.f10 ../cameo/
	cp 4.k08 ../cameo/
	cp 5.k08 ../cameo/
	cp 6.k08 ../cameo/

view: test
	feh ./out.png
