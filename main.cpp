#include "patch.hpp"

#include <glm/gtc/matrix_transform.hpp>

#include <algorithm>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

size_t resolution = 512 / 4;
size_t count = resolution * resolution;
float mat_size = 150.f;

float r1 = 9;
float r1a = 8;
int step1count = 13;
float r2 = 7;
int step2count = 17;
float r3 = 5;
float r4 = 4;
float r5 = 4;
float r6 = 4;

bool doPath1 = true;
bool doPath2 = true;
bool doPath3 = true;
bool doPath4 = true;
bool doPath5 = true;
bool doPath6 = true;


std::vector<float> biases = {-1, 1, -1, 1, 1, 1, 1, 1};

int SpaceToPic(float coord, int res = resolution) {
	coord += mat_size / 2;
	coord /= mat_size;
	coord *= res - 1;
	int ret = (int) coord;
	if (ret < 0) ret = 0;
	if (ret >= res) ret = res - 1;
	return ret;
}

float cross(glm::vec2 a, glm::vec2 b) {
	return a.x * b.y - a.y * b.x;
}

bool Intersect(glm::vec2 p, glm::vec2 pp, glm::vec2 q, glm::vec2 qq, glm::vec2 *out) {
	glm::vec2 r = pp - p;
	glm::vec2 s = qq - q;
	float t = cross(q - p, s) / cross(r, s);
	float u = cross(q - p, r) / cross(r, s);
	if (t >= 0 && t < 1 && u >= 0 && u < 1) {
		*out = p + t * r;
		return true;
	}
	return false;
}

float PicToSpace(int coord, int res = resolution) {
	float ret = coord;
	ret /= res - 1;
	ret *= mat_size;
	ret -= mat_size / 2;
	return ret;
}

void SaveImage(float *data, const char* name, int res = resolution, float height = 50) {
	uint8_t *data2 = (uint8_t*)malloc(res * res * sizeof(*data2));
	for (int i = 0; i < res * res; i++) {
		data2[i] = (data[i] / height) * 255;
	}
	stbi_write_png(name, res, res, 1, data2, 0);
	free(data2);
}

void makePath1(std::vector<Patch> patches) {
	float *pic = (float*)malloc(resolution * resolution * sizeof(*pic));
	for (size_t i = 0; i < count; i++) {
		pic[i] = 20.f;
	}
	for (int i = 0; i < patches.size(); i++) {
		for (float u = 0; u < patches[i].width; u += 0.005f) {
			for (float v = 0; v < patches[i].height; v += 0.005f) {
				glm::vec3 pos = EvaluateDistant2(patches[i], {u, v}, r1, biases[i]);
				int x = SpaceToPic(pos.x);
				int y = SpaceToPic(pos.y);
				int o = y * resolution + x;
				float z = pos.z;
				if (pic[o] < z) pic[o] = z;
			}
		}
		for (int u = 0; u <= patches[i].width; u++) {
			for (float v = 0; v < patches[i].height; v += 0.05f) {
				glm::vec3 pos = Evaluate(patches[i], {u, v});
				int xstart = SpaceToPic(pos.x - r1) - 1;
				int xend = SpaceToPic(pos.x + r1) + 2;
				int ystart = SpaceToPic(pos.y - r1) - 1;
				int yend = SpaceToPic(pos.y + r1) + 2;
				if (xstart < 0) xstart = 0;
				if (xend > resolution) xend = resolution;
				if (ystart < 0) ystart = 0;
				if (yend > resolution) yend = resolution;
				for (int uu = xstart; uu < xend; uu++) {
					for (int vv = ystart; vv < yend; vv++) {
						float x = PicToSpace(uu);
						float y = PicToSpace(vv);
						float dx = x - pos.x;
						float dy = y - pos.y;
						float rr = dx * dx + dy * dy;
						if (rr > r1 * r1) continue;
						float dz = sqrt(r1 * r1 - rr);
						float z = pos.z + dz;
						int o = vv * resolution + uu;
						if (pic[o] < z) pic[o] = z;
					}
				}
			}
		}
		for (int v = 0; v <= patches[i].height; v++) {
			for (float u = 0; u < patches[i].width; u += 0.05f) {
				glm::vec3 pos = Evaluate(patches[i], {u, v});
				int xstart = SpaceToPic(pos.x - r1) - 1;
				int xend = SpaceToPic(pos.x + r1) + 2;
				int ystart = SpaceToPic(pos.y - r1) - 1;
				int yend = SpaceToPic(pos.y + r1) + 2;
				if (xstart < 0) xstart = 0;
				if (xend > resolution) xend = resolution;
				if (ystart < 0) ystart = 0;
				if (yend > resolution) yend = resolution;
				for (int uu = xstart; uu < xend; uu++) {
					for (int vv = ystart; vv < yend; vv++) {
						float x = PicToSpace(uu);
						float y = PicToSpace(vv);
						float dx = x - pos.x;
						float dy = y - pos.y;
						float rr = dx * dx + dy * dy;
						if (rr > r1 * r1) continue;
						float dz = sqrt(r1 * r1 - rr);
						float z = pos.z + dz;
						int o = vv * resolution + uu;
						if (pic[o] < z) pic[o] = z;
					}
				}
			}
		}
	}
	SaveImage(pic, "out.png");

	std::vector<glm::vec3> path1;
	path1.push_back({0, 0, 80});
	path1.push_back({-75, -75, 80});
	path1.push_back({-75, -75, 35});

	float last_height = 35;
	float path_y = -75;
	float path_x = -75;
	int dir = 1;

	while (true) {
		for (int i = 0; i < resolution - 1; i++) {
			path_x = -75 * dir + 150.f * dir * i / (resolution - 1) + 0.01;
			int xx = SpaceToPic(path_x);
			int yy = SpaceToPic(path_y);
			float height = pic[yy * resolution + xx] - r1a;
			if (height < 35) height = 35;
			if (height != last_height) {
				if (last_height == 35) path1.push_back({PicToSpace(xx - dir), path_y, last_height});
				path1.push_back({path_x, path_y, height});
			}
			last_height = height;
		}
		path_x = 75 * dir;
		int xx = SpaceToPic(path_x);
		int yy = SpaceToPic(path_y);
		float height = pic[yy * resolution + xx] - r1a;
		if (height < 35) height = 35;
		path1.push_back({path_x, path_y, height});
		last_height = height;
		dir *= -1;
		float next_y = path_y + mat_size / step1count;
		if (next_y > 76) break;
		int y2 = SpaceToPic(next_y);
		int steps = y2 - yy;
		float temp_y;
		for (int i = 0; i < steps - 1; i++) {
			temp_y = path_y + mat_size / step1count * i / (steps - 1);
			int xx = SpaceToPic(path_x);
			int yy = SpaceToPic(temp_y);
			float height = pic[yy * resolution + xx] - r1a;
			if (height < 35) height = 35;
			if (height != last_height) path1.push_back({path_x, temp_y, height});
			last_height = height;
		}
		path_y = next_y;
		xx = SpaceToPic(path_x);
		yy = SpaceToPic(next_y);
		height = pic[yy * resolution + xx] - r1a;
		if (height < 35) height = 35;
		path1.push_back({path_x, path_y, height});
		last_height = height;
	}

	while (true) {
		for (int i = 0; i < resolution - 1; i++) {
			path_x = -75 * dir + 150.f * dir * i / (resolution - 1) + 0.01;
			int xx = SpaceToPic(path_x);
			int yy = SpaceToPic(path_y);
			float height = pic[yy * resolution + xx] - r1a;
			if (height < 21) height = 21;
			if (height != last_height) {
				if (last_height == 21) path1.push_back({PicToSpace(xx - dir), path_y, last_height});
				path1.push_back({path_x, path_y, height});
			}
			last_height = height;
		}
		path_x = 75 * dir;
		int xx = SpaceToPic(path_x);
		int yy = SpaceToPic(path_y);
		float height = pic[yy * resolution + xx] - r1a;
		if (height < 21) height = 21;
		path1.push_back({path_x, path_y, height});
		last_height = height;
		dir *= -1;
		float next_y = path_y - mat_size / step1count;
		if (next_y < -76) break;
		int y2 = SpaceToPic(next_y);
		int steps = yy - y2;
		float temp_y;
		for (int i = 0; i < steps - 1; i++) {
			temp_y = path_y - mat_size / step1count * i / (steps - 1);
			int xx = SpaceToPic(path_x);
			int yy = SpaceToPic(temp_y);
			float height = pic[yy * resolution + xx] - r1a;
			if (height < 21) height = 21;
			if (height != last_height) path1.push_back({path_x, temp_y, height});
			last_height = height;
		}
		path_y = next_y;
		xx = SpaceToPic(path_x);
		yy = SpaceToPic(next_y);
		height = pic[yy * resolution + xx] - r1a;
		if (height < 21) height = 21;
		path1.push_back({path_x, path_y, height});
		last_height = height;
	}
	path1.push_back({-75, -75, 80});
	path1.push_back({0, 0, 80});

	FILE *file1 = fopen("1.k16", "w");
	for (int i = 0; i < path1.size(); i++) {
		auto p = path1[i];
		fprintf(file1, "N%dG01X%.3fY%.3fZ%.3f\n", i, p.x, p.y, p.z);
	}
	fclose(file1);
	free(pic);
}

void makePath2(std::vector<Patch> patches) {
	float *pic = (float*)malloc(resolution * resolution * sizeof(*pic));
	for (size_t i = 0; i < count; i++) {
		pic[i] = 20.f;
	}
	for (int i = 0; i < patches.size(); i++) {
		for (float u = 0; u < patches[i].width; u += 0.005f) {
			for (float v = 0; v < patches[i].height; v += 0.005f) {
				glm::vec3 pos = EvaluateDistant2(patches[i], {u, v}, r2, biases[i]);
				int x = SpaceToPic(pos.x);
				int y = SpaceToPic(pos.y);
				int o = y * resolution + x;
				float z = pos.z;
				if (pic[o] < z) pic[o] = z;
			}
		}
		for (int u = 0; u <= patches[i].width; u++) {
			for (float v = 0; v < patches[i].height; v += 0.05f) {
				glm::vec3 pos = Evaluate(patches[i], {u, v});
				int xstart = SpaceToPic(pos.x - r2) - 1;
				int xend = SpaceToPic(pos.x + r2) + 2;
				int ystart = SpaceToPic(pos.y - r2) - 1;
				int yend = SpaceToPic(pos.y + r2) + 2;
				if (xstart < 0) xstart = 0;
				if (xend > resolution) xend = resolution;
				if (ystart < 0) ystart = 0;
				if (yend > resolution) yend = resolution;
				for (int uu = xstart; uu < xend; uu++) {
					for (int vv = ystart; vv < yend; vv++) {
						float x = PicToSpace(uu);
						float y = PicToSpace(vv);
						float dx = x - pos.x;
						float dy = y - pos.y;
						float rr = dx * dx + dy * dy;
						if (r2 > r2 * r2) continue;
						float dz = sqrt(r2 * r2 - rr);
						float z = pos.z + dz;
						int o = vv * resolution + uu;
						if (pic[o] < z) pic[o] = z;
					}
				}
			}
		}
	}

	std::vector<glm::vec3> path2;
	path2.push_back({0, 0, 80});
	path2.push_back({-75, -85, 80});
	path2.push_back({-75, -85, 20});
	path2.push_back({-75, 75, 20});

	int u, v, next_u;
	float path_x, path_y, next_x;
	bool done = false;
	path_x = -75;
	path_y = 75;
	int flag = 0;

path2thing:
	while (true) {
		u = SpaceToPic(path_x);
		v = SpaceToPic(path_y);
		next_x = path_x + mat_size / step2count;
		next_u = SpaceToPic(next_x);
		while (u != next_u) {
			u++;
			if (u == resolution) {
				next_x = 75;
				u--;
				break;
			}
			float h = pic[v * resolution + u];
			if (h != 20.f) {
				u--;
				next_x = PicToSpace(u);
				done = true;
				break;
			}
		}
		path_x = next_x;
		path2.push_back({path_x, path_y, 20});
		if (done) break;
		while (true) {
			if (!v) break;
			float h = pic[v * resolution + u];
			if (h != 20.f) {
				v++;
				break;
			}
			v--;
		}
		path_y = PicToSpace(v);
		path2.push_back({path_x, path_y, 20});
		next_x = path_x + mat_size / step2count;
		next_u = SpaceToPic(next_x);
		while (u != next_u) {
			u++;
			if (u == resolution) {
				next_x = 75;
				u--;
				break;
			}
			float h = pic[v * resolution + u];
			while (h == 20.f) {
				v--;
				if (v <= 0) {
					break;
				}
				h = pic[v * resolution + u];
			}
			while (h != 20.f) {
				v++;
				if (v >= resolution) {
					v--;
					next_x = PicToSpace(u);
					done = true;
					break;
				}
				h = pic[v * resolution + u];
			}
			float temp_x = PicToSpace(u);
			if (v < 0) v = 0;
			path_y = PicToSpace(v);
			path2.push_back({temp_x, path_y, 20});
			if (done) break;
			if (v < 0) {
				v = 0;
				path_y = PicToSpace(v);
				break;
			}
		}
		path_x = next_x;
		path2.push_back({path_x, path_y, 20});
		if (done) break;
		path_y = 75;
		path2.push_back({path_x, path_y, 20});
		if (path_x > 70) break;
	}

	if (!flag) {
		flag++;
		done = false;
		path_y += 15;
		path2.push_back({path_x, path_y, 20});
		v = SpaceToPic(75);
		u++;
		float h = pic[v * resolution + u];
		while (h != 20.f) {
			u++;
			h = pic[v * resolution + u];
		}
		path_x = PicToSpace(u);
		path2.push_back({path_x, path_y, 20});
		path_y = 75;
		path2.push_back({path_x, path_y, 20});

		goto path2thing;
	}

	path2.push_back({85, 75, 20});
	path2.push_back({85, -75, 20});
	path_x = 75 - mat_size / step2count;
	path_y = -75;
	path2.push_back({path_x, path_y, 20});

path2thing2:
	while (true) {
		u = SpaceToPic(path_x);
		v = SpaceToPic(path_y);
		next_x = path_x - mat_size / step2count;
		next_u = SpaceToPic(next_x);
		while (u != next_u) {
			u--;
			if (u < 0) {
				next_x = -75;
				u++;
				break;
			}
			float h = pic[v * resolution + u];
			if (h != 20.f) {
				u++;
				next_x = PicToSpace(u);
				done = true;
				break;
			}
		}
		path_x = next_x;
		path2.push_back({path_x, path_y, 20});
		if (done) break;
		while (true) {
			if (v == resolution - 1) break;
			float h = pic[v * resolution + u];
			if (h != 20.f) {
				v--;
				break;
			}
			v++;
		}
		path_y = PicToSpace(v);
		path2.push_back({path_x, path_y, 20});
		next_x = path_x - mat_size / step2count;
		next_u = SpaceToPic(next_x + 1);
		while (u != next_u) {
			u--;
			if (!u) {
				next_x = -75;
				break;
			}
			float h = pic[v * resolution + u];
			while (h == 20.f) {
				v++;
				if (v >= resolution - 1) {
					break;
				}
				h = pic[v * resolution + u];
			}
			while (h != 20.f) {
				v--;
				if (!v) {
					next_x = PicToSpace(u);
					done = true;
					break;
				}
				h = pic[v * resolution + u];
			}
			float temp_x = PicToSpace(u);
			if (v < 0) v = 0;
			if (v == 0) break;
			path_y = PicToSpace(v);
			path2.push_back({temp_x, path_y, 20});
			if (done) break;
			if (v < 0) {
				v = 0;
				path_y = PicToSpace(v);
				break;
			}
		}
		path_x = next_x;
		path2.push_back({path_x, path_y, 20});
		if (done) break;
		path_y = -75;
		path2.push_back({path_x, path_y, 20});
		if (path_x < -70) break;
	}

	if (flag) {
		flag--;
		done = false;
		path_y -= 10;
		path2.push_back({path_x, path_y, 20});
		path_x -= mat_size / step2count / 2;
		path2.push_back({path_x, path_y, 20});
		path_y = -75;
		path2.push_back({path_x, path_y, 20});
		goto path2thing2;
	}

	path2.push_back({-75, -85, 20});

	glm::vec3 temp_pos = path2[path2.size() - 1];
	path2.push_back({temp_pos.x, temp_pos.y, 80});
	path2.push_back({0, 0, 80});

	FILE *file2 = fopen("2.f12", "w");
	for (int i = 0; i < path2.size(); i++) {
		auto p = path2[i];
		fprintf(file2, "N%dG01X%.3fY%.3fZ%.3f\n", i, p.x, p.y, p.z);
	}
	fclose(file2);

	SaveImage(pic, "out.png");

	free(pic);
}

int goAlong(int along, int intersect, int start, bool down, std::vector<glm::vec3> &path3, std::vector<std::vector<glm::vec2>> &paths, glm::vec2 last, bool ff = true) {
	float path_x = last.x;
	float path_y = last.y;
	float next_x, next_y;
	int startidx = 0;
	int i = start;
	bool first = ff;
	while (down ? (i >= 0) : (i < paths[along].size() - 1)) {
		auto a = paths[along][i];
		next_x = a.x;
		next_y = a.y;
		bool flag = false;
		if ((path_x - next_x) * (path_x - next_x) + (path_y - next_y) * (path_y - next_y) > 0.1) {
			int j = intersect;
			if (first) {
				first = false;
			} else {
				for (int k = 0; k < paths[j].size() - 1; k++) {
					glm::vec2 a = {paths[j][k].x, paths[j][k].y};
					glm::vec2 b = {paths[j][k+1].x, paths[j][k+1].y};
					glm::vec2 out;
					if (Intersect({path_x, path_y}, {next_x, next_y}, a, b, &out)) {
						flag = true;
						path_x = out.x;
						path_y = out.y;
						startidx = k;
						path3.push_back({path_x, path_y, 20});
						break;
					}
				}
			}
			if (flag) break;
			path_x = next_x;
			path_y = next_y;
			path3.push_back({path_x, path_y, 20});
		}
		down ? i-- : i++;
	}
	return startidx;
}

void makePath3(std::vector<Patch> patches) {
	float *pic = (float*)malloc(resolution * resolution * sizeof(*pic));
	for (size_t i = 0; i < count; i++) {
		pic[i] = 20.f;
	}
	for (int i = 0; i < patches.size(); i++) {
		for (float u = 0; u < patches[i].width; u += 0.05f) {
			for (float v = 0; v < patches[i].height; v += 0.05f) {
				glm::vec3 pos = Evaluate(patches[i], {u, v});
				int x = SpaceToPic(pos.x);
				int y = SpaceToPic(pos.y);
				int o = y * resolution + x;
				float z = pos.z;
				if (pic[o] < z) pic[o] = z;
			}
		}
	}

	std::vector<int> vindices = {0,   0,   1, 4, 4, 5, 5, 6};
	std::vector<float> vs =     {0.5, 1.5, 1, 2, 5, 0, 2, 1};

	std::vector<std::vector<glm::vec2>> paths;

	for (int i = 0; i < vindices.size(); i++) {
		std::vector<glm::vec2> path;
		for(float u = 0.001; u < patches[vindices[i]].height; u += 0.001f) {
			glm::vec3 pos = EvaluateDistant2(patches[vindices[i]], {vs[i], u}, r3, biases[vindices[i]]);
			int x = SpaceToPic(pos.x);
			int y = SpaceToPic(pos.y);
			int o = y * resolution + x;
			pic[o] = 50;
			path.push_back({pos.x, pos.y});
		}
		paths.push_back(path);
	}

	std::vector<glm::vec3> path3;

	path3.push_back({0, 0, 80});
	auto start = Evaluate(patches[0], {0, 0});
	path3.push_back({-85, start.y, 80});
	path3.push_back({-85, start.y, 20});
	float angle = 0;
	float x = 0;
	float y = 0;
	auto target = paths[0][0];
	float path_x, path_y;
	float next_x, next_y;

	do {
		x = -cos(angle) * r3;
		y = sin(angle) * r3;
		path_x = start.x + x;
		path_y = start.y + y;
		path3.push_back({path_x, path_y, 20});
		angle += 0.01;
	} while ((path_x - target.x) * (path_x - target.x) + (path_y - target.y) * (path_y - target.y) > 0.1);

	for (auto a : paths[0]) {
		next_x = a.x;
		next_y = a.y;
		if ((path_x - next_x) * (path_x - next_x) + (path_y - next_y) * (path_y - next_y) > 0.1) {
			path_x = next_x;
			path_y = next_y;
			path3.push_back({path_x, path_y, 20});
		}
	}

	path_x = next_x;
	path_y = next_y;
	path3.push_back({path_x, path_y, 20});

	target = {path_x, path_y};
	start = Evaluate(patches[0], {0, patches[0].height});

	do {
		angle -= 0.01;
		x = -cos(angle) * r3;
		y = sin(angle) * r3;
		path_x = start.x + x;
		path_y = start.y + y;
	} while ((path_x - target.x) * (path_x - target.x) + (path_y - target.y) * (path_y - target.y) > 0.1);

	target = paths[1][paths[1].size() - 1];

	do {
		x = -cos(angle) * r3;
		y = sin(angle) * r3;
		path_x = start.x + x;
		path_y = start.y + y;
		path3.push_back({path_x, path_y, 20});
		angle += 0.01;
	} while ((path_x - target.x) * (path_x - target.x) + (path_y - target.y) * (path_y - target.y) > 0.1);

	int startidx = goAlong(1, 5, paths[1].size() - 1, true, path3, paths, {path_x, path_y});
	path_x = paths[5][startidx].x;
	path_y = paths[5][startidx].y;
	startidx = goAlong(5, 4, startidx, true, path3, paths, {path_x, path_y});
	path_x = paths[4][startidx].x;
	path_y = paths[4][startidx].y;
	startidx = goAlong(4, 2, startidx + 1, false, path3, paths, {path_x, path_y});
	path_x = paths[2][startidx].x;
	path_y = paths[2][startidx].y;
	startidx = goAlong(2, 4, startidx + 1, false, path3, paths, {path_x, path_y});
	path_x = paths[4][startidx].x;
	path_y = paths[4][startidx].y;
	startidx = goAlong(4, 7, startidx + 1, false, path3, paths, {path_x, path_y});
	startidx = paths[3].size() - 1;
	path_x = paths[3][startidx].x;
	path_y = paths[3][startidx].y;
	startidx = goAlong(3, 7, startidx, true, path3, paths, {path_x, path_y}, false);
	path_x = paths[7][startidx].x;
	path_y = paths[7][startidx].y;
	startidx = goAlong(7, 3, startidx, true, path3, paths, {path_x, path_y});
	path_x = paths[3][startidx].x;
	path_y = paths[3][startidx].y;
	startidx = goAlong(3, 7, startidx, true, path3, paths, {path_x, path_y}, false);
	path_x = paths[3][0].x;
	path_y = paths[3][0].y;

	target = {path_x, path_y};
	start = Evaluate(patches[4], {0, 0});

	do {
		angle -= 0.01;
		x = -cos(angle) * r3;
		y = sin(angle) * r3;
		path_x = start.x + x;
		path_y = start.y + y;
	} while ((path_x - target.x) * (path_x - target.x) + (path_y - target.y) * (path_y - target.y) > 0.1);

	target = paths[4][0];

	do {
		x = -cos(angle) * r3;
		y = sin(angle) * r3;
		path_x = start.x + x;
		path_y = start.y + y;
		path3.push_back({path_x, path_y, 20});
		angle += 0.01;
	} while ((path_x - target.x) * (path_x - target.x) + (path_y - target.y) * (path_y - target.y) > 0.1);

	startidx = goAlong(4, 6, 0, false, path3, paths, {path_x, path_y});
	path_x = paths[6][startidx].x;
	path_y = paths[6][startidx].y;
	startidx = goAlong(6, 1, startidx, false, path3, paths, {path_x, path_y});
	path_x = paths[1][startidx].x;
	path_y = paths[1][startidx].y;
	startidx = goAlong(1, 0, startidx, true, path3, paths, {path_x, path_y});

	start = Evaluate(patches[0], {0, 0});
	target = paths[1][0];

	do {
		angle -= 0.01;
		x = -cos(angle) * r3;
		y = sin(angle) * r3;
		path_x = start.x + x;
		path_y = start.y + y;
	} while ((path_x - target.x) * (path_x - target.x) + (path_y - target.y) * (path_y - target.y) > 0.1);

	do {
		path3.push_back({path_x, path_y, 20});
		angle += 0.01;
		x = -cos(angle) * r3;
		y = sin(angle) * r3;
		path_x = start.x + x;
		path_y = start.y + y;
	} while (y < 0);
	angle = 0;
	x = -cos(angle) * r3;
	y = sin(angle) * r3;
	path_x = start.x + x;
	path_y = start.y + y;
	path3.push_back({path_x, path_y, 20});

	path3.push_back({-85, start.y, 20});
	path3.push_back({-85, start.y, 80});
	path3.push_back({0, 0, 80});

	FILE *file3 = fopen("3.f10", "w");
	for (int i = 0; i < path3.size(); i++) {
		auto p = path3[i];
		fprintf(file3, "N%dG01X%.3fY%.3fZ%.3f\n", i, p.x, p.y, p.z);
	}
	fclose(file3);

	SaveImage(pic, "out.png");
	free(pic);
}

void mark(std::vector<glm::vec3> &path, glm::vec2 p, float h = 20) {
	path.push_back({p.x, p.y, 80});
	path.push_back({p.x, p.y, h});
	path.push_back({p.x, p.y, 80});
}

void makePath4(std::vector<Patch> patches) {
	int multiplier = 16;
	float step = 0.002f;
	int res = resolution * multiplier;

	float *pic = (float*)malloc(res * res * sizeof(*pic));

	std::vector<glm::vec3> path3;
	path3.push_back({0, 0, 80});

	
	std::vector<float> density = {30, 30, 30, 14, 18, 14, 15};
	//std::vector<float> epsilon = {-0.5, -0.5, -0.4, -0.5, -0.4, -0.5, -0.3};
	std::vector<float> epsilon = {-0.1, -0.1, -0.1, -0.1, -0.1, -0.1, -0.1};
	std::vector<float> steps = {0.05, 0.05, 0.05, 0.1, 0.05, 0.05, 0.05};
	std::vector<bool> alongU = {true, true, true, true, true, true, true};


	for (int ii = 0; ii < patches.size(); ii++) {
		if (ii == 3) continue;
		bool upwards = true;
		bool detached = true;
		glm::vec3 lastPos = {0, 0, 0};


		{ // STUPID IDEA START
			for (size_t i = 0; i < res * res; i++) {
				pic[i] = 24.f;
			}
			for (int i = 0; i < patches.size(); i++) {
				if (i == ii) continue;
				if (i == 3) continue;

				if (ii == 4 && i == 1) step /= 4;

				for (float u = 0; u < patches[i].width; u += step) {
					for (float v = 0; v < patches[i].height; v += step) {
						glm::vec3 pos = EvaluateDistant2(patches[i], {u, v}, r4, biases[i]);
						int x = SpaceToPic(pos.x, res);
						int y = SpaceToPic(pos.y, res);
						int o = y * res + x;
						float z = pos.z;
						if (pic[o] < z) pic[o] = z;
					}
				}
				for (int u = 0; u <= patches[i].width; u++) {
					for (float v = 0; v < patches[i].height; v += step) {
						glm::vec3 pos = Evaluate(patches[i], {u, v});
						int xstart = SpaceToPic(pos.x - r4, res) - 1;
						int xend = SpaceToPic(pos.x + r4, res) + 2;
						int ystart = SpaceToPic(pos.y - r4, res) - 1;
						int yend = SpaceToPic(pos.y + r4, res) + 2;
						if (xstart < 0) xstart = 0;
						if (xend > res) xend = res;
						if (ystart < 0) ystart = 0;
						if (yend > res) yend = res;
						for (int uu = xstart; uu < xend; uu++) {
							for (int vv = ystart; vv < yend; vv++) {
								float x = PicToSpace(uu, res);
								float y = PicToSpace(vv, res);
								float dx = x - pos.x;
								float dy = y - pos.y;
								float rr = dx * dx + dy * dy;
								if (rr > r4 * r4) continue;
								float dz = sqrt(r4 * r4 - rr);
								float z = pos.z + dz;
								int o = vv * res + uu;
								if (pic[o] < z) pic[o] = z;
							}
						}
					}
				}
				for (int v = 0; v <= patches[i].height; v++) {
					for (float u = 0; u < patches[i].width; u += step) {
						glm::vec3 pos = Evaluate(patches[i], {u, v});
						int xstart = SpaceToPic(pos.x - r4, res) - 1;
						int xend = SpaceToPic(pos.x + r4, res) + 2;
						int ystart = SpaceToPic(pos.y - r4, res) - 1;
						int yend = SpaceToPic(pos.y + r4, res) + 2;
						if (xstart < 0) xstart = 0;
						if (xend > res) xend = res;
						if (ystart < 0) ystart = 0;
						if (yend > res) yend = res;
						for (int uu = xstart; uu < xend; uu++) {
							for (int vv = ystart; vv < yend; vv++) {
								float x = PicToSpace(uu, res);
								float y = PicToSpace(vv, res);
								float dx = x - pos.x;
								float dy = y - pos.y;
								float rr = dx * dx + dy * dy;
								if (rr > r4 * r4) continue;
								float dz = sqrt(r4 * r4 - rr);
								float z = pos.z + dz;
								int o = vv * res  + uu;
								if (pic[o] < z) pic[o] = z;
							}
						}
					}
				}
				if (ii == 4 && i == 1) step *= 4;
			}
		} // STUPID IDEA END



		bool veryFirst = true;
		for (float u = 0; u <= patches[ii].width; u += 1 / density[ii]) {
			float v = upwards ? 0 : patches[ii].height;
			bool first = true;
			for (;;) {
				if (upwards && v > patches[ii].height) break;
				if (!upwards && v < 0) break;

				float ut = u;

				if (ii == 4 || ii == 6) {
					ut += 3;
					if (ut > patches[ii].width) ut -= patches[ii].width;
				}

				glm::vec3 pos = EvaluateDistant2(patches[ii], {ut, v}, r4, biases[ii]);
				int x = SpaceToPic(pos.x, res);
				int y = SpaceToPic(pos.y, res);
				float z = pic[y * res + x];

				if (z - pos.z < epsilon[ii]) {
					if (detached) {
						glm::vec3 temp = path3.back();
						glm::vec3 dist = temp - pos;
						float ddd = sqrt(dist.x * dist.x + dist.y * dist.y);
						detached = false;

						bool extraTest = false;

						if (ii == 1 && u > 1.5) {
							extraTest = true;
						}

						float dtest = 7;

						if (ii == 4 && v > 3) dtest = 3;
						if (ii == 6) dtest = 5;

						if ((first && !veryFirst && ddd < dtest && !extraTest) || ddd < 0.7) {
							path3.pop_back();
						} else {
							path3.push_back({pos.x, pos.y, 50});
						}
						first = false;
						veryFirst = false;
					}
					float height = pos.z - r4;
					if (height < 20) height = 20;
					path3.push_back({pos.x, pos.y, height});
				} else {
					if (!detached) {
						detached = true;
						path3.push_back({lastPos.x, lastPos.y, 50});
					}

				}

				lastPos = pos;

				v += upwards ? steps[ii] : -steps[ii];
			}

			upwards = !upwards;
		 }




		if (ii == 1) {

			bool first = true;

			/*
			mark(path3, {38, 37});
			mark(path3, {55, 37});
			mark(path3, {38, 53});
			mark(path3, {55, 53});
			*/
			float step = 0.001f;
			float safety = 0.2f;

			bool upward = true;
			for (int x = 38; x < 56; x++) {
				upward = !upward;
				float y_start = 0;
				float y_end = 0;
				for (float y = 37; y < 54; y+= step) {
					int xx = SpaceToPic(x, res);
					int yy = SpaceToPic(y, res);
					int o = yy * res  + xx;
					if (pic[o] == 24.f) {
						if (!y_start) y_start = y;
					} else {
						if (y_start && !y_end) y_end = y-step;
					}
				}
				if (!y_start) continue;
				y_start += safety;
				y_end -= safety;
				if (y_start >= y_end) continue;
				if (!upward) {
					float temp = y_start;
					y_start = y_end;
					y_end = temp;
				}
				if (first) {
					first = false;
					path3.push_back({x, y_start, 50});
				}
				path3.push_back({x, y_start, 20});
				path3.push_back({x, y_end, 20});
			}
			glm::vec3 last = path3.back();
			path3.push_back({last.x, last.y, 25});

			first = true;
			for (int y = 37; y < 54; y++) {
				upward = !upward;
				float x_start = 0;
				float x_end = 0;
				for (float x = 38; x < 56; x+=step) {
					int xx = SpaceToPic(x, res);
					int yy = SpaceToPic(y, res);
					int o = yy * res  + xx;
					if (pic[o] == 24.f) {
						if (!x_start) x_start = x;
					} else {
						if (x_start && !x_end) x_end = x-step;
					}
				}
				if (!x_start) continue;
				x_start += safety;
				x_end -= safety;
				if (x_start >= x_end) continue;
				if (!upward) {
					float temp = x_start;
					x_start = x_end;
					x_end = temp;
				}
				if (first) {
					first = false;
					path3.push_back({x_start, y, 25});
				}
				path3.push_back({x_start, y, 20});
				path3.push_back({x_end, y, 20});
			}
			last = path3.back();
			path3.push_back({last.x, last.y, 50});

		}
	}

	path3.push_back({0, 0, 80});

	FILE *file4 = fopen("4.k08", "w");
	for (int i = 0; i < path3.size(); i++) {
		auto p = path3[i];
		fprintf(file4, "N%dG01X%.3fY%.3fZ%.3f\n", i, p.x, p.y, p.z);
	}
	fclose(file4);

	free(pic);
}

void makePath5(std::vector<Patch> patches) {
	std::vector<glm::vec3> path5;
	path5.push_back({0, 0, 80});

	bool first;
	glm::vec3 last;

	glm::vec3 start = {28, 48, 21};
	auto test = Intersect(patches[4], patches[1], r5, biases[4], biases[1], start, 1000, true, 400);
	std::reverse(std::begin(test), std::end(test));
	first = true;
	for (auto y : test) {
		glm::vec3 x = y - glm::vec3(0, 0, r5);
		if (x.z <= 20) continue;
		if (first) {
			first = false;
			path5.push_back({x.x, x.y, 80});
		}
		path5.push_back(x);
		last = x;
	}
	path5.push_back({last.x, last.y, 25});

	start = {53, 53, 20};
	test = Intersect(patches[4], patches[6], r5, biases[4], biases[6], start, 900, true, 800, 400);
	std::reverse(std::begin(test), std::end(test));
	first = true;
	for (auto y : test) {
		glm::vec3 x = y - glm::vec3(0, 0, r5);
		if (x.z <= 20) continue;
		if (first) {
			first = false;
			path5.push_back({x.x, x.y, 25});
		}
		path5.push_back(x);
		last = x;
	}
	path5.push_back({last.x, last.y, 25});

	start = {38, 38, 20};
	test = Intersect(patches[4], patches[6], r5, biases[4], biases[6], start, 1000);
	first = true;
	for (auto y : test) {
		glm::vec3 x = y - glm::vec3(0, 0, r5);
		if (x.z <= 20) continue;
		if (first) {
			first = false;
			path5.push_back({x.x, x.y, 25});
		}
		path5.push_back(x);
		last = x;
	}
	path5.push_back({last.x, last.y, 30});

	//r5 -= 0.1;

	start = {-25, 5, 20};
	test = Intersect(patches[5], patches[0], r5, biases[5], biases[0], start, 1000);
	first = true;
	for (auto y : test) {
		glm::vec3 x = y - glm::vec3(0, 0, r5);
		if (x.z <= 20) continue;
		if (first) {
			first = false;
			path5.push_back({x.x, x.y, 30});
		}
		path5.push_back(x);
		last = x;
	}
	path5.push_back({last.x, last.y, 25});

	//r5 += 0.1;

	start = {-25, -5, 20};
	test = Intersect(patches[4], patches[5], r5, biases[4], biases[5], start, 1000);
	first = true;
	for (auto y : test) {
		glm::vec3 x = y - glm::vec3(0, 0, r5);
		if (x.z <= 20) continue;
		if (first) {
			first = false;
			path5.push_back({x.x, x.y, 25});
		}
		path5.push_back(x);
		last = x;
	}
	path5.push_back({last.x, last.y, 30});


	r5 = 4;

	//mark(path5, {-28, -28}, 45);

	first = true;
	for (float v = patches[0].height - 0.001; v > 0; v -= 0.02) {
		glm::vec3 pos = Evaluate(patches[0], {0, v});
		glm::vec3 n1 = EvaluateNormal(patches[0], {0.0001, v});
		glm::vec3 n2 = EvaluateNormal(patches[0], {3.9999, v});
		glm::vec3 n = glm::normalize(n1 + n2);
		pos += n * r5;
		pos -= glm::vec3(0, 0, r5);
		if (pos.z < 20) continue;
		if (first) {
			path5.push_back({pos.x, pos.y, 30});
			first = false;
		}
		last = pos;
		path5.push_back(pos);
	}
	path5.push_back({last.x, last.y, 50});

	first = true;
	for (float v = 0.2; v < 1.91; v += 0.001) {
		glm::vec3 pos = Evaluate(patches[2], {0, v});
		glm::vec3 n1 = EvaluateNormal(patches[2], {0.0001, v});
		glm::vec3 n2 = EvaluateNormal(patches[2], {1.9999, v});
		glm::vec3 n = glm::normalize(n1 + n2);
		pos += n * r5;
		pos -= glm::vec3(0, 0, r5);
		if (pos.z < 20) continue;
		if (first) {
			path5.push_back({pos.x, pos.y, 50});
			first = false;
		}
		last = pos;
		path5.push_back(pos);
	}
	path5.push_back({last.x, last.y, 40});

	patches[2].wrapped = false;
	start = {-25, -25, 44.9};
	test = Intersect(patches[2], patches[4], r5, biases[2], biases[4], start, 1300, true, 600);
	first = true;
	for (auto y : test) {
		glm::vec3 x = y - glm::vec3(0, 0, r5);
		//if (x.z <= 20) continue;
		if (first) {
			first = false;
			path5.push_back({x.x, x.y, 40});
		}
		path5.push_back(x);
		last = x;
	}
	path5.push_back({last.x, last.y, 50});


	start = {-20, -5, 35};
	//mark(path5, {start.x, start.y}, start.z);
	test = Intersect(patches[2], patches[4], r5, biases[2], biases[4], start, 1300, true, 300);
	first = true;
	for (auto y : test) {
		glm::vec3 x = y - glm::vec3(0, 0, r5);
		//if (x.z <= 20) continue;
		if (first) {
			first = false;
			path5.push_back({x.x, x.y, 50});
		}
		path5.push_back(x);
		last = x;
	}
	path5.push_back({last.x, last.y, 80});



	path5.push_back({0, 0, 80});

	FILE *file5 = fopen("5.k08", "w");
	for (int i = 0; i < path5.size(); i++) {
		auto p = path5[i];
		fprintf(file5, "N%dG01X%.3fY%.3fZ%.3f\n", i, p.x, p.y, p.z);
	}
	fclose(file5);

}

std::vector<glm::vec2> WalkAround(Patch p, float start_u, float ustep, float vstep, float r, float bias) {
	std::vector<glm::vec2> ret;

	for (float v = 0; v < p.height + vstep; v += vstep) {
		bool found = false;
		glm::vec3 fpos;

		float u = start_u;

		for (;;) {
			glm::vec3 pos = EvaluateDistant2(p, {u, v}, r, bias);
			if (pos.z < 20 + r) break;
			if (std::isnan(pos.x)) break;
			found = true;
			fpos = pos;
			u += ustep;
			if (u < 0) u += p.width;
			if (u >= p.width) u -= p.width;
		}

		if (found) ret.push_back({fpos.x, fpos.y});
	}

	return ret;
}

void makePath6(std::vector<Patch> patches) {
	/*
	float *pic = (float*)malloc(resolution * resolution * sizeof(*pic));
	for (size_t i = 0; i < count; i++) {
		pic[i] = 24.f;
	}
	for (int i = 0; i < patches.size(); i++) {
		for (float u = 0; u < patches[i].width; u += 0.005f) {
			for (float v = 0; v < patches[i].height; v += 0.005f) {
				glm::vec3 pos = Evaluate(patches[i], {u, v});
				int x = SpaceToPic(pos.x);
				int y = SpaceToPic(pos.y);
				int o = y * resolution + x;
				float z = pos.z;
				if (pic[o] < z) pic[o] = z;
			}
		}
	}

	int qqq = 6;

	for (float v = 0; v < patches[qqq].height; v += 0.01) {
		glm::vec3 pos = Evaluate(patches[qqq], {1., v});
		if (pos.z < 20) continue;
		int x = SpaceToPic(pos.x);
		int y = SpaceToPic(pos.y);
		int o = y * resolution + x;
		pic[o] = 50;
	}

	SaveImage(pic, "out.png");
	*/

	std::vector<std::vector<glm::vec2>> paths;

	paths.push_back(WalkAround(patches[0], 0., 0.0001, 0.02, r6, biases[0]));
	paths.push_back(WalkAround(patches[0], 2., -0.0001, 0.02, r6, biases[0]));
	paths.push_back(WalkAround(patches[1], 1.5, -0.0001, 0.02, r6, biases[1]));
	paths.push_back(WalkAround(patches[4], 0.5, 0.0001, 0.02, r6, biases[4]));
	paths.push_back(WalkAround(patches[4], 0.5, -0.0001, 0.02, r6, biases[4]));
	paths.push_back(WalkAround(patches[5], 3., 0.0001, 0.02, r6, biases[5]));
	paths.push_back(WalkAround(patches[5], 3., -0.0001, 0.02, r6, biases[5]));
	paths.push_back(WalkAround(patches[6], 0., 0.0001, 0.02, r6, biases[6]));
	paths.push_back(WalkAround(patches[6], 0., -0.0001, 0.02, r6, biases[6]));

	std::vector<glm::vec3> path6;

	path6.push_back({0, 0, 80});

	path6.push_back({paths[0].front(), 50});

	float path_x = 0;
	float path_y = 0;
	int startidx = goAlong(0, 5, 0, false, path6, paths, {path_x, path_y});

	path6.push_back({paths[0].back(), 25});
	path6.push_back({paths[1].back(), 25});

	startidx = goAlong(1, 5, paths[1].size() - 1, true, path6, paths, {path_x, path_y});
	path_x = paths[5][startidx].x;
	path_y = paths[5][startidx].y;
	startidx = goAlong(5, 4, startidx, true, path6, paths, {path_x, path_y});
	path_x = paths[4][startidx].x;
	path_y = paths[4][startidx].y;
	startidx = goAlong(4, 2, startidx, false, path6, paths, {path_x, path_y});
	path_x = paths[2][startidx].x;
	path_y = paths[2][startidx].y;
	startidx = goAlong(2, 4, startidx, false, path6, paths, {path_x, path_y});
	path6.push_back({path6.back().x, path6.back().y, 30});

	std::vector<glm::vec3> trash;
	startidx = goAlong(3, 8, paths[3].size() - 1, true, trash, paths, {path_x, path_y});
	path_x = paths[8][startidx].x;
	path_y = paths[8][startidx].y;
	path6.push_back({trash.back().x, trash.back().y, 30});
	startidx = goAlong(8, 3, startidx, true, path6, paths, {path_x, path_y});
	path_x = paths[3][startidx].x;
	path_y = paths[3][startidx].y;
	startidx = goAlong(3, 8, startidx, false, path6, paths, {path_x, path_y});
	path6.push_back({path6.back().x, path6.back().y, 25});

	path6.push_back({paths[3].back(), 25});

	startidx = goAlong(3, 7, paths[3].size() - 1, true, path6, paths, {path_x, path_y});
	path_x = paths[7][startidx].x;
	path_y = paths[7][startidx].y;
	startidx = goAlong(7, 3, startidx, true, path6, paths, {path_x, path_y});
	path_x = paths[3][startidx].x;
	path_y = paths[3][startidx].y;
	startidx = goAlong(3, 1, startidx, true, path6, paths, {path_x, path_y});

	path6.push_back({path6.back().x, path6.back().y, 25});
	path6.push_back({paths[4][0], 25});

	startidx = goAlong(4, 6, 0, false, path6, paths, {path_x, path_y});
	path_x = paths[6][startidx].x;
	path_y = paths[6][startidx].y;
	startidx = goAlong(6, 1, startidx, false, path6, paths, {path_x, path_y});
	path_x = paths[1][startidx].x;
	path_y = paths[1][startidx].y;
	startidx = goAlong(1, 6, startidx, true, path6, paths, {path_x, path_y});
	path6.push_back({path6.back().x, path6.back().y, 50});

	path6.push_back({0, 0, 80});

	FILE *file6 = fopen("6.k08", "w");
	for (int i = 0; i < path6.size(); i++) {
		auto p = path6[i];
		fprintf(file6, "N%dG01X%.3fY%.3fZ%.3f\n", i, p.x, p.y, p.z);
	}
	fclose(file6);
}

int main(int argc, char **argv) {
	glm::mat4 transform = glm::mat4(1.f);
	transform = glm::rotate(transform, glm::pi<float>() / 4, {0, 0, 1});
	transform = glm::translate(transform, {-35, -25, 20});
	transform = glm::scale(transform, {20, 20, 30});

	auto patches = LoadModel(argv[1], transform);

	if (doPath1) makePath1(patches);
	if (doPath2) makePath2(patches);
	if (doPath3) makePath3(patches);
	if (doPath4) makePath4(patches);
	if (doPath5) makePath5(patches);
	if (doPath6) makePath6(patches);

	return 0;
}
