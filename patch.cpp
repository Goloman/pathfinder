#include "patch.hpp"

#include "json.hpp"

#include <fstream>
#include <algorithm>

std::vector<Patch> LoadModel(std::string filename, glm::mat4 transform) {
	std::vector<Patch> ret;

	std::ifstream i;
	nlohmann::json j;

	try {
		i.open(filename);
		i >> j;
	} catch (...) {
		return ret;
	}

	std::vector<glm::vec3> points;

	auto items = j["points"];
	for (auto p : items) {
		float x, y, z;
		x = p["x"].get<float>();
		y = p["y"].get<float>();
		z = p["z"].get<float>();
		glm::vec4 h = {x, y, z, 1};
		h = transform * h;
		points.push_back({h.x, h.y, h.z});
	}

	items = j["surfacesC0"];
	for (auto p: items) {
		Patch patch;
		patch.smooth = false;
		patch.wrapped = p["cylinder"].get<bool>();
		auto px = p["points"];
		patch.height = (px.size() - 1) / 3;
		for (auto py : px) {
			patch.width = (py.size() - 1) / 3;
			for (auto point : py) {
				patch.points.push_back(points[point.get<int>()]);
			}
		}
		ret.push_back(patch);
	}

	items = j["surfacesC2"];
	for (auto p: items) {
		Patch patch;
		patch.smooth = true;
		patch.wrapped = p["cylinder"].get<bool>();
		auto px = p["points"];
		patch.height = px.size() - 3;
		for (auto py : px) {
			patch.width = py.size() - 3;
			for (auto point : py) {
				patch.points.push_back(points[point.get<int>()]);
			}
		}
		ret.push_back(patch);
	}

	return ret;
}

static glm::vec4 Bernstein3(float x) {
    glm::vec4 ret;
    float ix = 1 - x;
    ret[0] = ix * ix * ix;
    ret[1] = 3 * x * ix * ix;
    ret[2] = 3 * x * x * ix;
    ret[3] = x * x * x;
    return ret;
}

static glm::vec4 Bernstein3d(float x) {
    glm::vec4 ret;
    float ix = 1 - x;
    ret[0] = -3 * ix * ix;
    ret[1] = 3 * ix * ix - 6 * x * ix;
    ret[2] = 6 * x * ix - 3 * x * x;
    ret[3] = 3 * x * x;
    return ret;
}

static glm::vec4 BSpline3(float x) {
    glm::vec4 ret;
    float ix = 1 - x;
    ret[0] = ix * ix * ix / 6.f;
    ret[1] = (3.f * x  - 6.f) * x * x + 4;
    ret[1] /= 6.f;
    ret[2] = ix * 3 * x * x + 3 * x + 1;
    ret[2] /= 6.f;
    ret[3] = x * x * x / 6.f;
    return ret;
}

static glm::vec4 BSpline3d(float x) {
    glm::vec4 ret;
    float ix = 1 - x;
    ret[0] = -3 * ix * ix / 6.f;
    ret[1] = 3 * x * (3 * x - 4);
    ret[1] /= 6.f;
    ret[2] = - 9 * x * x + 6 * x + 3;
    ret[2] /= 6.f;
    ret[3] = 3 * x * x / 6.f;
    return ret;
}

static void PrepareCoordinates(Patch patch, glm::vec2 uv, int *w, int *h, float *u, float *v) {
	*w = floor(uv.x);
	*h = floor(uv.y);
	*u = uv.x - *w;
	*v = uv.y - *h;

	if (*w < 0) {
		*w = 0;
		*u = 0;
	} else if (*w >= patch.width) {
		*w = patch.width - 1;
		*u = 1;
	}

	if (*h < 0) {
		*h = 0;
		*v = 0;
	} else if (*h >= patch.height) {
		*h = patch.height - 1;
		*v = 1;
	}
}

static void GetControlPoints(Patch patch, int w, int h, glm::mat4 *xs, glm::mat4 *ys, glm::mat4 *zs) {
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			glm::vec3 pos;
			if (patch.smooth) {
				pos = patch.points[w + i + (patch.width + 3) * (h + j)];
			} else {
				pos = patch.points[3 * w + i + (3 * patch.width + 1) * (h * 3 + j)];
			}
			(*xs)[i][j] = pos.x;
			(*ys)[i][j] = pos.y;
			(*zs)[i][j] = pos.z;
		}
	}
}


glm::vec3 Evaluate(Patch patch, glm::vec2 uv) {
	int w, h;
	float u, v;
	PrepareCoordinates(patch, uv, &w, &h, &u, &v);
	glm::mat4 xs, ys, zs;
	GetControlPoints(patch, w, h, &xs, &ys, &zs);
	glm::vec4 tu, tv;
	if (patch.smooth) {
		tu = BSpline3(u);
		tv = BSpline3(v);
	} else {
		tu = Bernstein3(u);
		tv = Bernstein3(v);
	}
	float x = glm::dot(tv * xs, tu);
	float y = glm::dot(tv * ys, tu);
	float z = glm::dot(tv * zs, tu);
	return {x, y, z};
}

glm::vec3 EvaluateDu(Patch patch, glm::vec2 uv) {
	int w, h;
	float u, v;
	PrepareCoordinates(patch, uv, &w, &h, &u, &v);
	glm::mat4 xs, ys, zs;
	GetControlPoints(patch, w, h, &xs, &ys, &zs);
	glm::vec4 tu, tv;
	if (patch.smooth) {
		tu = BSpline3d(u);
		tv = BSpline3(v);
	} else {
		tu = Bernstein3d(u);
		tv = Bernstein3(v);
	}
	float x = glm::dot(tv * xs, tu);
	float y = glm::dot(tv * ys, tu);
	float z = glm::dot(tv * zs, tu);
	return {x, y, z};
}

glm::vec3 EvaluateDv(Patch patch, glm::vec2 uv) {
	int w, h;
	float u, v;
	PrepareCoordinates(patch, uv, &w, &h, &u, &v);
	glm::mat4 xs, ys, zs;
	GetControlPoints(patch, w, h, &xs, &ys, &zs);
	glm::vec4 tu, tv;
	if (patch.smooth) {
		tu = BSpline3(u);
		tv = BSpline3d(v);
	} else {
		tu = Bernstein3(u);
		tv = Bernstein3d(v);
	}
	float x = glm::dot(tv * xs, tu);
	float y = glm::dot(tv * ys, tu);
	float z = glm::dot(tv * zs, tu);
	return {x, y, z};
}

glm::vec3 EvaluateNormal(Patch patch, glm::vec2 uv) {
	glm::vec3 normal = glm::normalize(glm::cross(EvaluateDu(patch, uv), EvaluateDv(patch, uv)));
	if (normal.z < 0) normal *= -1;
	return normal;
}

glm::vec3 EvaluateNormal2(Patch patch, glm::vec2 uv) {
	glm::vec3 normal = glm::normalize(glm::cross(EvaluateDu(patch, uv), EvaluateDv(patch, uv)));
	return normal;
}

glm::vec3 EvaluateDistant(Patch patch, glm::vec2 uv, float radius) {
	glm::vec3 pos = Evaluate(patch, uv);
	glm::vec3 norm = EvaluateNormal(patch, uv);
	return pos + norm * radius;
}

glm::vec3 EvaluateDistant2(Patch patch, glm::vec2 uv, float radius, float bias) {
	glm::vec3 pos = Evaluate(patch, uv);
	glm::vec3 norm = EvaluateNormal2(patch, uv);
	return pos + norm * radius * bias;
}

glm::vec2 UvFromPos(Patch patch, glm::vec3 pos, float radius, float bias) {
	float dist = std::numeric_limits<float>::max();
	glm::vec2 best = {0, 0};

	for (float u = 0; u < patch.width; u+= 0.01) {
		for (float v = 0; v < patch.height; v+= 0.01) {
			glm::vec3 p = EvaluateDistant2(patch, {u, v}, radius, bias);
			glm::vec3 diff = pos - p;
			float d2 = glm::dot(diff, diff);
			if (d2 < dist) {
				dist = d2;
				best = {u, v};
			}
		}
	}

	return best;
}

glm::vec4 Gradient(Patch p1, Patch p2, glm::vec2 uv1, glm::vec2 uv2, float radius, float bias1, float bias2) {
	glm::vec3 e1 = EvaluateDistant2(p1, uv1, radius, bias1);
	glm::vec3 e2 = EvaluateDistant2(p2, uv2, radius, bias2);
	glm::vec3 ed = e1 - e2;

	glm::vec3 u1 = EvaluateDu(p1, uv1);
	glm::vec3 v1 = EvaluateDv(p1, uv1);
	float u1l = glm::dot(u1, u1);
	float v1l = glm::dot(v1, v1);

	glm::vec3 u2 = EvaluateDu(p2, uv2);
	glm::vec3 v2 = EvaluateDv(p2, uv2);
	float u2l = glm::dot(u2, u2);
	float v2l = glm::dot(v2, v2);

	u1 /= u1l;
	v1 /= v1l;
	u2 /= u2l;
	v2 /= v2l;

	return glm::vec4(glm::dot( ed, u1),
	                 glm::dot( ed, v1),
	                 glm::dot(-ed, u2),
	                 glm::dot(-ed, v2));
}

glm::mat4 Jacobi(Patch p1, Patch p2, glm::vec2 uv1, glm::vec2 uv2, glm::vec3 normal) {
	glm::vec3 du1 = EvaluateDu(p1, uv1);
	glm::vec3 dv1 = EvaluateDv(p1, uv1);
	glm::vec3 du2 = EvaluateDu(p2, uv2);
	glm::vec3 dv2 = EvaluateDv(p2, uv2);

	glm::vec4 c0 = {du1, glm::dot(du1, normal)};
	glm::vec4 c1 = {dv1, glm::dot(dv1, normal)};
	glm::vec4 c2 = {-du2, 0};
	glm::vec4 c3 = {-dv2, 0};

	glm::mat4 ret = {c0, c1, c2, c3};

	return ret;
}

glm::bvec2 IsOutside(Patch p, glm::vec2 &uv) {
	glm::bvec2 ret = {false, false};
	if (uv.x < 0) {
		if (p.wrapped) {
			uv.x += p.width;
		} else {
			uv.x = 0;
			ret.x = true;
		}
	}
	if (uv.x >= p.width) {
		if (p.wrapped) {
			uv.x -= p.width;
		} else {
			uv.x = p.width - 0.0001f;
			ret.x = true;
		}
	}
	if (uv.y < 0) ret.y = true;
	if (uv.y >= p.height) ret.y = true;
	return ret;
}

std::vector<glm::vec3> Intersect(Patch p1, Patch p2, float radius, float bias1, float bias2, glm::vec3 start, int count, bool both, int skip, int count2) {
	glm::vec2 uv1 = UvFromPos(p1, start, radius, bias1);
	glm::vec2 uv2 = UvFromPos(p2, start, radius, bias2);

	std::vector<glm::vec3> intersection;

	bool out = false;

	for (int i = 0; i < 10; i++) {
		glm::vec4 grad = Gradient(p1, p2, uv1, uv2, radius, bias1, bias2);
		uv1 -= glm::vec2(grad.x, grad.y);
		uv2 -= glm::vec2(grad.z, grad.w);
	}

	glm::vec2 startuv1 = {0, 0};
	glm::vec2 startuv2 = {0, 0};

	for (int i = 0; i < count; i++) {
		glm::vec3 a1 = EvaluateDistant2(p1, uv1, radius, bias1);
		glm::vec3 a2 = EvaluateDistant2(p2, uv2, radius, bias2);
		glm::vec3 pos = (a1 + a2) / 2.f;
		if (i >= skip) {
			intersection.push_back(pos);
		}
		if (i == skip) {
			startuv1 = uv1;
			startuv2 = uv2;
		}
		glm::vec3 n1 = EvaluateNormal2(p1, uv1);
		n1 = glm::normalize(n1);
		glm::vec3 n2 = EvaluateNormal2(p2, uv2);
		n2 = glm::normalize(n2);
		glm::vec3 n = glm::normalize(glm::cross(n1, n2));
		float d = glm::dot(n, pos);
		d += 0.5;
		glm::bvec4 outside = {false, false, false, false};
		for (int j = 0; j < 3; j++) {
			glm::mat4 inv = glm::inverse(Jacobi(p1, p2, uv1, uv2, n));
			glm::vec3 a1 = EvaluateDistant2(p1, uv1, radius, bias1);
			glm::vec3 a2 = EvaluateDistant2(p2, uv2, radius, bias2);
			float dist = glm::dot(a1 - n*d, n);
			glm::vec4 value = {a1 - a2, dist};
			glm::vec4 delta = inv * value;
			delta *= 0.02f;
			uv1 -= glm::vec2(delta.x, delta.y);
			uv2 -= glm::vec2(delta.z, delta.w);
			auto t1 = IsOutside(p1, uv1);
			auto t2 = IsOutside(p2, uv2);
			outside = {t1, t2};
			if (glm::any(outside)) {
				out = true;
				break;
			}
		}
		if (out) break;
	}

	if (!both) return intersection;

	std::reverse(std::begin(intersection), std::end(intersection));
	out = false;
	intersection.pop_back();

	uv1 = startuv1;
	uv2 = startuv2;

	if (count2) count = count2;

	for (int i = 0; i < count; i++) {
		glm::vec3 a1 = EvaluateDistant2(p1, uv1, radius, bias1);
		glm::vec3 a2 = EvaluateDistant2(p2, uv2, radius, bias2);
		glm::vec3 pos = (a1 + a2) / 2.f;
		intersection.push_back(pos);
		glm::vec3 n1 = EvaluateNormal2(p1, uv1);
		n1 = glm::normalize(n1);
		glm::vec3 n2 = EvaluateNormal2(p2, uv2);
		n2 = glm::normalize(n2);
		glm::vec3 n = glm::normalize(glm::cross(n1, n2));
		float d = glm::dot(n, pos);
		d -= 0.5;
		glm::bvec4 outside = {false, false, false, false};
		for (int j = 0; j < 3; j++) {
			glm::mat4 inv = glm::inverse(Jacobi(p1, p2, uv1, uv2, n));
			glm::vec3 a1 = EvaluateDistant2(p1, uv1, radius, bias1);
			glm::vec3 a2 = EvaluateDistant2(p2, uv2, radius, bias2);
			float dist = glm::dot(a1 - n*d, n);
			glm::vec4 value = {a1 - a2, dist};
			glm::vec4 delta = inv * value;
			delta *= 0.02f;
			uv1 -= glm::vec2(delta.x, delta.y);
			uv2 -= glm::vec2(delta.z, delta.w);
			auto t1 = IsOutside(p1, uv1);
			auto t2 = IsOutside(p2, uv2);
			outside = {t1, t2};
			if (glm::any(outside)) {
				out = true;
				break;
			}
		}
		if (out) break;
	}

	return intersection;
}
